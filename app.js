const express = require('express');
const mysql = require('mysql');
const databaseConnection = require('./database/databaseConnection');
const createRoutes = require('./routes/createRoutes')
const consoleLogger = require('./logger/consoleLogger');
const bodyParser = require('body-parser');
require('./logger/appCrashLogger');

const app = express();
app.use(bodyParser.json());

app.listen('3000', () => {
    console.log('server started on port 3000');
});

databaseConnection.connect((err) => {
    if(err){
        consoleLogger('mysql connection error....');
        consoleLogger(err);
        throw err;
    }
    else{
        consoleLogger('mysql connection success....');
        createRoutes(app);
    }
});