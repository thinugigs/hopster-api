function appCrashLogger(){
    require('crashreporter').configure({
        outDir: './crashLogginFiles', // default to cwd
        exitOnCrash: true, // if you want that crash reporter exit(1) for you, default to true,
        maxCrashFile: 100 // older files will be removed up, default 5 files are kept
    });
}
module.exports = appCrashLogger();