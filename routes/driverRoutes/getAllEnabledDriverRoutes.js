const databaseConnection = require('../../database/databaseConnection');
const mysql = require('mysql');

require('../../logger/appCrashLogger');

function getAllEnabledDriverRoutesRoute(app){
    app.post('/getAllEnabledDriverRoutes',(req, res) => {
        getAllEnabledDriverRoutes(req, res);
    });
}

function getAllEnabledDriverRoutes(req, res){
    var sql = mysql.format("select * from driver_routes r, users u where r.userId = u.u_userId AND isEnabled=?", ["true"]);

    databaseConnection.query(sql, function (error, results, fields) {
        if (error) {
            res.status(200).send(error);
        }
        res.status(200).send(results);
    });
}

module.exports = getAllEnabledDriverRoutesRoute;