const databaseConnection = require('../../database/databaseConnection');
const mysql = require('mysql');

require('../../logger/appCrashLogger');

function insertNewDriverRouteRoute(app){
    app.post('/insertNewDriverRoute',(req, res) => {
        createNewUser(req, res);
    });
}

function insertNewDriverRoute(req, res){
    var values = {
        fromLocation: req.body.fromLocation,
        fromLatitude: req.body.fromLatitude,
        fromLongitude: req.body.fromLongitude,
        toLocation: req.body.toLocation,
        toLatitude: req.body.toLatitude,
        toLongitude: req.body.toLongitude,
        frequency: req.body.frequency,
        time: req.body.time,
        distance: req.body.distance,
        isEnabled: req.body.isEnabled,
        userId: req.body.userId,
        polylinePoints: req.body.polylinePoints,
        tripDate: req.body.tripDate
    };
    const sql = 'INSERT INTO driver_routes SET ?';
    databaseConnection.query(sql, values, function (error, results, fields) {
        if (error) {
            res.status(400).send({
                status: false,
                result: error
            });
        }
        try {
            var waypoints = JSON.parse(req.body.waypoints)
            if (waypoints.length > 0) {

                for (let waypoint of waypoints) {
                    var values = {
                        routeId: results.insertId,
                        location: waypoint.location,
                        latitude: waypoint.latitude,
                        longitude: waypoint.longitude,
                        waypointPosition: waypoint.waypointPosition
                    };
                }

                databaseConnection.query('INSERT INTO driver_waypoints SET ?', values, function (error, result, fields) {
                    
                });

                res.status(200).send({
                    status: true,
                    result: results
                });

            } else {
                res.status(200).send({
                    status: true,
                    result: results
                });
            }
        } catch (error) {
			console.log("error")
        }
    });
}

module.exports = insertNewDriverRouteRoute;