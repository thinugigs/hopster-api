const databaseConnection = require('../../database/databaseConnection');
const mysql = require('mysql');

require('../../logger/appCrashLogger');

function deleteRouteRoute(app){
    app.post('/deleteRoute',(req, res) => {
        deleteRoute(req, res);
    });
}

function deleteRoute(req, res){
    var sql = mysql.format("DELETE FROM driver_routes where id=?", [req.body.tripId]);

    databaseConnection.query(sql, function (error, results, fields) {
        if (error) {
            console.log(error)
            res.status(200).send(error);
        }

        res.status(200).send({
            status: true,
            user: results
        });
    });
}

module.exports = deleteRouteRoute;