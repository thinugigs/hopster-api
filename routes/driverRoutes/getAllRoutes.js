const databaseConnection = require('../../database/databaseConnection');
const mysql = require('mysql');

require('../../logger/appCrashLogger');

function getAllRoutesRoute(app){
    app.post('/getAllRoutes',(req, res) => {
        getAllRoutes(req, res);
    });
}

function getAllRoutes(req, res){
    var sql = mysql.format("select * from driver_routes where userId=?", [req.body.userId]);

    databaseConnection.query(sql, function (error, results, fields) {
        if (error) {
            res.status(200).send(error);
        }
        res.status(200).send(results);
    });
}

module.exports = getAllRoutesRoute;