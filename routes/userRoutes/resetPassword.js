const databaseConnection = require('../../database/databaseConnection');
const mysql = require('mysql');

require('../../logger/appCrashLogger');

function resetPasswordRoute(app){
    app.post('/resetPassword',(req, res) => {
        resetPassword(req, res);
    });
}

function resetPassword(req, res){
    const email = req.body.email;
    const password = req.body.password;
    var sql = mysql.format("update users SET u_password = ? where u_email=?", [password, email]);
    databaseConnection.query(sql, function (err, result, fields) {
        if (err) {
            res.status(400).send({
                status: false
            });
        }

        if (result.affectedRows == 0) {
            res.status(200).send({
                status: false,
                user: result
            });
        } else {
            res.status(200).send({
                status: true,
                user: result
            });
        }		
    });
}

module.exports = resetPasswordRoute;