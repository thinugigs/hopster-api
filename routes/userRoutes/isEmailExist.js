const databaseConnection = require('../../database/databaseConnection');
const mysql = require('mysql');

require('../../logger/appCrashLogger');

function isEmailExistRoute(app){
    app.post('/isEmailExist',(req, res) => {
        isEmailExist(req, res);
    });
}

function isEmailExist(req, res){
    const email = req.body.email;
    var sql = mysql.format("SELECT * from users where u_email=?", [email]);
    databaseConnection.query(sql, function (err, result, fields) {
        if (err){
            res.status(200).send(err);
        }
        else if(result.length > 0) {
            res.status(200).send({
                status: true,
                user: result
            });
        }
        else {
            res.status(200).send({
                status: false
            });
        }
    });
}

module.exports = isEmailExistRoute;