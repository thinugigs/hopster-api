const databaseConnection = require('../../database/databaseConnection');
const mysql = require('mysql');

require('../../logger/appCrashLogger');

function createNewUserRoute(app){
    app.post('/createNewUser',(req, res) => {
        createNewUser(req, res);
    });
}

function createNewUser(req, res){
    var values = {
        u_firstName: req.body.firstName,
        u_lastName: req.body.lastName,
        u_contactNumber: req.body.contactNumber,
        u_email: req.body.email,
        u_password: req.body.password,
        u_nicNumber: req.body.nicNumber,
        u_gender: req.body.gender
    }
    const sql = 'INSERT INTO users SET ?';
    databaseConnection.query(sql, values, function (err, result, fields) {
        if (err){
            res.status(400).send({
                status: false,
                result: err
            });
        }
        else{
            res.status(200).send({
				status: true,
				result: result
			});
        }
    });
}

module.exports = createNewUserRoute;