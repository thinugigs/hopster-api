const databaseConnection = require('../../database/databaseConnection');
const mysql = require('mysql');

require('../../logger/appCrashLogger');

function loginRoute(app){
    app.post('/login',(req, res) => {
        login(req, res);
    });
}

function login(req, res){
    const email = req.body.email;
    const password = req.body.password;
    const sql = 'SELECT * FROM users WHERE u_email = ' + mysql.escape(email) + 'AND u_password = ' + mysql.escape(password);
    databaseConnection.query(sql, function (err, result, fields) {
        if (err){
            res.status(200).send(err);
        }
        else if(result.length > 0) {
            res.status(200).send({
                status: true,
                user: result
            });
        }
        else {
            res.status(200).send({
                status: false
            });
        }
    });
}

module.exports = loginRoute;