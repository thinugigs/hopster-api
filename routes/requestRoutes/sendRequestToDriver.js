const databaseConnection = require('../../database/databaseConnection');
const mysql = require('mysql');
var admin = require("firebase-admin");

require('../../logger/appCrashLogger');

var serviceAccount = require("../../configs/prepgamification-firebase-adminsdk-apdlr-0519f1626a.json");

function sendRequestToDriverRoute(app){
    app.post('/sendRequestToDriver',(req, res) => {
        sendRequestToDriver(req, res);
    });
}

function sendRequestToDriver(req, res){
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://prepgamification.firebaseio.com"
    });

    var driverRouteId = req.body.driverRouteId;
	var passengerFrom = req.body.passengerFrom;
	var passengerTo = req.body.passengerTo;
	var time = req.body.time;
	var tripDate = req.body.tripDate;
	var driverId = req.body.driverId;
	var passengerId = req.body.passengerId;

	var driverFirebaseToken = ""

	
		// Insert into route_requests table
		var values = {
			routeId: req.body.driverRouteId,
			passengerId: req.body.passengerId,
			driverId: req.body.driverId,
			isAccepted: 0
		};

		databaseConnection.query('INSERT INTO route_requests SET ?', values, function (error, requestResults, fields) {
			if (error) {
				res.status(400).send({
					status: false,
					result: error
				});
			}

			// Send push notification to driver
			var sql = mysql.format("select * from users where u_userId=?", [driverId]);
			
			databaseConnection.query(sql, function (error, results, fields) {
				if (error) {
					res.status(200).send(error);
				}
	
				driverFirebaseToken = results[0]["firebaseToken"];
	
				var payload = {
					notification: {
						title: "New Request",
						body: "Please open notification to accept request"
					},
					data: {
						passengerFrom: passengerFrom,
						passengerTo: passengerTo,
						time: time,
						tripDate: tripDate,
						requestId: String(requestResults.insertId)
					}
				};
	
				var options = {
					priority: "high",
					timeToLive: 60 * 60 * 24
				};
	
				admin.messaging().sendToDevice(driverFirebaseToken, payload, options).then(function (response) {
					console.log("Successfully sent push notification");
	
					res.status(200).send({
						status: true,
						response: response
					});
				}).catch(function (error) {
					console.log("Error sending message:", error);
	
					res.status(400).send({
						status: false,
						response: error
					});
				});
			});
		});		
}

module.exports = sendRequestToDriverRoute;``