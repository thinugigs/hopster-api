const databaseConnection = require('../../database/databaseConnection');
const mysql = require('mysql');
var admin = require("firebase-admin");

require('../../logger/appCrashLogger');

var serviceAccount = require("../../configs/prepgamification-firebase-adminsdk-apdlr-0519f1626a.json");

function updateFirebaseTockenRoute(app){
    app.post('/updateFirebaseTocken',(req, res) => {
        updateFirebaseTocken(req, res);
    });
}

function updateFirebaseTocken(req, res){
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://prepgamification.firebaseio.com"
    });

    var sql = mysql.format("update users SET u_firebaseToken = ? where userId=?", [req.body.firebaseToken, req.body.userId]);

    databaseConnection.query(sql, function (error, results, fields) {
        if (error) {
            res.status(400).send({
                status: false
            });
        }

        if (results.affectedRows == 0) {
            res.status(200).send({
                status: false,
                user: results
            });
        } else {
            res.status(200).send({
                status: true,
                user: results
            });
        }
    });
}

module.exports = updateFirebaseTockenRoute;