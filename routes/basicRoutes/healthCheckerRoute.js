
function healthCheckerRoute(app){
    app.get('/healthchecker',(req, res) => {
        healthChecker(req, res);
    });
}

function healthChecker(req, res){
    res.status(200).send({
        status: true
    });
}

module.exports = healthCheckerRoute;