const loginRoute = require('./userRoutes/loginRoute');
const createNewUser = require('./userRoutes/createNewUser');
const isEmailExist = require('./userRoutes/isEmailExist');
const resetPassword = require('./userRoutes/resetPassword');

const insertNewDriverRoute = require('./driverRoutes/insertNewDriverRoute');
const getAllRoutes = require('./driverRoutes/getAllRoutes');
const deleteRoute = require('./driverRoutes/deleteRoute');

const updateFirebaseTocken = require('./requestRoutes/updateFirebaseTocken');
const sendRequestToDriver = require('./requestRoutes/sendRequestToDriver');

const healthCheckerRoute = require('./basicRoutes/healthCheckerRoute');

require('../logger/appCrashLogger');

function createRoutes(app){
    
    healthCheckerRoute(app);
    loginRoute(app);
    createNewUser(app);
    isEmailExist(app);
    resetPassword(app);
    insertNewDriverRoute(app);
    getAllRoutes(app);
    deleteRoute(app);
    updateFirebaseTocken(app);
    sendRequestToDriver(app);
}

module.exports = createRoutes;