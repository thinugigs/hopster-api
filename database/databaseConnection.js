const mysql = require('mysql');
const databaseValues = require('./databaseValues.json');

require('../logger/appCrashLogger');

const databaseConnectionValues = databaseValues.databaseConnectionValues;

const databaseConnection = mysql.createConnection({
    host        : databaseConnectionValues.host,
    user        : databaseConnectionValues.user,
    password    : databaseConnectionValues.password,
    database    : databaseConnectionValues.database
});

module.exports = databaseConnection;