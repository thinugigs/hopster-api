-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 12, 2018 at 08:23 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hopster`
--

-- --------------------------------------------------------

--
-- Table structure for table `driver_routes`
--

CREATE TABLE `driver_routes` (
  `routeId` int(11) UNSIGNED NOT NULL,
  `fromLocation` varchar(300) DEFAULT NULL,
  `fromLatitude` varchar(300) DEFAULT NULL,
  `fromLongitude` varchar(300) DEFAULT NULL,
  `toLocation` varchar(300) DEFAULT NULL,
  `toLatitude` varchar(300) DEFAULT NULL,
  `toLongitude` varchar(300) DEFAULT NULL,
  `frequency` varchar(300) DEFAULT NULL,
  `time` varchar(300) DEFAULT NULL,
  `distance` varchar(300) DEFAULT NULL,
  `isEnabled` varchar(300) DEFAULT NULL,
  `tripDate` varchar(200) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `polylinePoints` varchar(10000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_routes`
--

INSERT INTO `driver_routes` (`routeId`, `fromLocation`, `fromLatitude`, `fromLongitude`, `toLocation`, `toLatitude`, `toLongitude`, `frequency`, `time`, `distance`, `isEnabled`, `tripDate`, `userId`, `polylinePoints`) VALUES
(72, 'Kaduwela', '6.9308305', '79.9842178', 'Pearson Lanka', '6.941507399999999', '79.8808706', 'Daily', '12:15 AM', '18.9 km', 'true', '', 27, 'suhi@_|dgNT~AJt@HDbBI\\Br@E|@E`@CZB`@n@HRBTEd@GPNBTFLH`@^lCpFd@r@|AxApFtExAhAhAp@|@d@d@VRDn@ZdB|@ZXHD|Ah@b@PbAh@nA|@t@j@\\`@NXNf@HhAJbA\\~@Xh@h@r@^X\\Rx@ZjBd@bAH|ABhBC~@Bd@HdA^f@Vd@\\b@b@r@pAn@`Ap@j@`@PZHz@LpBPlALfCH`B@rBEdBA|@Bz@JdARdD|@bCl@t@^^V\\Zp@fAfAvCz@fCJz@E`AGf@Ol@k@bBIh@A^@f@Fh@^`BHTPXjAr@vB`AvBfAjAx@j@f@r@x@Zr@fAvChAvC|@`Bb@n@h@`AhAlCn@fBj@hBx@jDb@rBLx@@j@A`@Mv@Q~@a@vAWfAM~@UtCC|@MvASjA]nAk@pBWxAC\\?j@H`Ad@fDBj@A\\Sj@mBzDyBxDOZARc@vA_B~Fw@lCQj@SV_@ZqAj@cAj@cAr@a@f@W^Qd@_@bBQ`AE^@z@Hh@Vv@Xz@L`ARrDDdC?`ECtAIhC?TAj@Ah@AxA@h@@TDb@ZbAvAvDt@zBRbANrADrB?rAI|FIlA]rB_@hBOv@EZ?TIb@Cn@BPFLZZNJLJNFZDb@DbBFp@Ld@Nj@Xd@Zd@`@PZF\\A\\oAtFSrAA`@BZN^VXx@j@v@l@b@h@nClDl@t@LRRh@H`@p@zEb@xC^bAR^dBlCPXPF|@`BJ~@@h@A`EBfGA~ACXAFANE`@E`@IdAE`@W`CeBhNUl@c@|@Ol@IvDOtCF|@~@zCLl@@`AIh@Ub@s@bAo@r@wApAcEpD_A`Ay@fAa@n@i@`BYdAM~@_@vC[pBq@xCOn@yAzD_A|Bc@v@oC~Ea@h@]r@{@pBO\\Kl@Cr@Dh@Nj@b@v@|@zAXj@Xt@\\|ARjCC`@G`@m@dEWpCeAzIe@rCYx@g@p@uCzBqAfAQVSx@Gn@CjCEvAA~IGzJEpAGn@Q|@Cb@OfUARO?sA?sOGkAK[Gm@CeJCmJEyMMq@AyIMaKEqHEmII{EImECuDG{MMsTQuCEkGAeDBmEGcGCqB@oBFcAFGUR?tAInBElBAfFBAQCi@Kq@@C`@Kn@_@X[JQODM?KIeCeDUSlB{Ao@{@'),
(73, 'Kaduwela', '6.9308305', '79.9842178', 'Orugodawatta', '6.945244499999998', '79.881483', 'One Time Trip', '8:14 PM', '18.7 km', 'true', '2018-04-19', 1, 'suhi@_|dgNT~AJt@HDbBI\\Br@E|@E`@CZB`@n@HRBTEd@GPNBTFLH`@^lCpFd@r@|AxApFtExAhAhAp@|@d@d@VRDn@ZdB|@ZXHD|Ah@b@PbAh@nA|@t@j@\\`@NXNf@HhAJbA\\~@Xh@h@r@^X\\Rx@ZjBd@bAH|ABhBC~@Bd@HdA^f@Vd@\\b@b@r@pAn@`Ap@j@`@PZHz@LpBPlALfCH`B@rBEdBA|@Bz@JdARdD|@bCl@t@^^V\\Zp@fAfAvCz@fCJz@E`AGf@Ol@k@bBIh@A^@f@Fh@^`BHTPXjAr@vB`AvBfAjAx@j@f@r@x@Zr@fAvChAvC|@`Bb@n@h@`AhAlCn@fBj@hBx@jDb@rBLx@@j@A`@Mv@Q~@a@vAWfAM~@UtCC|@MvASjA]nAk@pBWxAC\\?j@H`Ad@fDBj@A\\Sj@mBzDyBxDOZARc@vA_B~Fw@lCQj@SV_@ZqAj@cAj@cAr@a@f@W^Qd@_@bBQ`AE^@z@Hh@Vv@Xz@L`ARrDDdC?`ECtAIhC?TAj@Ah@AxA@h@@TDb@ZbAvAvDt@zBRbANrADrB?rAI|FIlA]rB_@hBOv@EZ?TIb@Cn@BPFLZZNJLJNFZDb@DbBFp@Ld@Nj@Xd@Zd@`@PZF\\A\\oAtFSrAA`@BZN^VXx@j@v@l@b@h@nClDl@t@LRRh@H`@p@zEb@xC^bAR^dBlCPXPF|@`BJ~@@h@A`EBfGA~ACXAFANE`@E`@IdAE`@W`CeBhNUl@c@|@Ol@IvDOtCF|@~@zCLl@@`AIh@Ub@s@bAo@r@wApAcEpD_A`Ay@fAa@n@i@`BYdAM~@_@vC[pBq@xCOn@yAzD_A|Bc@v@oC~Ea@h@]r@{@pBO\\Kl@Cr@Dh@Nj@b@v@|@zAXj@Xt@\\|ARjCC`@G`@m@dEWpCeAzIe@rCYx@g@p@uCzBqAfAQVSx@Gn@CjCEvAA~IGzJEpAGn@Q|@Cb@OfUARO?sA?sOGkAK[Gm@CeJCmJEyMMq@AyIMaKEqHEmII{EImECuDG{MMsTQuCEkGAeDBmEGcGCqB@oBFcAFGUi@{@K_@Iu@FQA]K_AQwAWaBSgBQqBKQ_@BcAFO@'),
(74, 'Kaduwela Bus Station', '6.9360717', '79.9831738', 'Borella Junction', '6.9117165', '79.87744889999999', 'One Time Trip', '12:10 AM', '15.9 km', 'true', '2018-04-19', 1, '_xii@kvdgNR}@NYlAeAh@c@XL\\Jx@PvARVHLLHNFZB`@At@B\\JXr@fAb@XRDpAFp@A\\ElBe@bAQz@B|AX`@FX?^?|@KvFu@TAXBp@RZVJN~@nBz@dBh@~@~A|A`DlCjDpC~A`ArAr@RDb@Rx@b@v@`@ZXj@TjA`@bAf@|AbAVRt@p@d@x@Jz@JvAZdAZl@b@n@v@n@|@^bBb@~@LbBD~AC`A?l@FbAZrAr@l@j@Zf@f@~@V^r@r@`@TTH|@NfBNdALjAFhCFrA?xCGx@@jAFhARtCv@xCr@p@Z|@l@Z^Xb@b@hAbBvENz@@ZC^Eh@If@m@hBK`@Gh@?f@Bd@\\~AVz@XVh@\\pB`AxBbAjAt@`Av@Z\\h@x@nAjDt@lB`@`A\\n@pApBhAfCzAhEv@zCj@fCRhAFn@?b@Gl@SfA[lA_@vAMz@Gh@OjBKzBQ|AzAn@`A^x@`@Ty@TqADwD?yA_DhBs@f@]PMvASjA]nAk@pBWxAC\\?j@H`Ad@fDBj@A\\Sj@mBzDyBxDOZARc@vA_B~Fw@lCQj@SV_@ZqAj@cAj@cAr@a@f@W^Qd@_@bBQ`AE^@z@Hh@Vv@Xz@L`ARrDDdC?`ECtAIhC?TATAj@?j@AvABh@Db@ZbAvAvDt@zBRbANrADrB?rAI|FIlA]rB_@hBOv@EZ?TIb@Cn@BPFLZZNJLJNFZDb@DbBFp@Ld@Nj@Xd@Zd@`@PZF\\A\\oAtFSrAA`@BZN^VXx@j@v@l@b@h@nClDl@t@LRRh@H`@p@zEb@xC^bAR^dBlCPXPF|@`BJ~@@h@A`EBfGA~ACXAFANCPC`@Eb@KbAYpCeBhNUl@c@|@Ol@IvDOtCF|@~@zCLl@@`AIh@Ub@s@bAo@r@wApAcEpD_A`Ay@fAa@n@i@`BYdAM~@_@vC[pBq@xCOn@yAzD_A|Bc@v@oC~Ea@h@]r@{@pBO\\Kl@Cr@Dh@Nj@b@v@|@zAXj@Xt@\\|ARjCC`@G`@m@dEWpCeAzIe@rCYx@g@p@uCzBqAfAQVSx@Gn@CjCEvAA~IGzJEpAGn@Q|@Cb@OfUARO?sA?'),
(75, 'Malabe Junction', '6.9039354', '79.9550129', 'Kaduwela', '6.9308305', '79.9842178', 'One Time Trip', '1:27 PM', '7.1 km', 'true', '2018-04-19', 1, 'slci@ye_gNGB@e@Bk@NkBFi@L{@^wAZmARgAFm@?c@Go@SiAk@gCw@{C{AiEiAgCqAqB]o@a@aAu@mBoAkDi@y@[]aAw@kAu@yBcAqBaAi@]YWW{@]_BCe@?g@Fi@Ja@l@iBHg@Di@B_@A[O{@cBwEc@iAYc@[_@}@m@q@[yCs@uCw@iASkAGy@AyCFsA?iCGkAGeAMgBO}@OUIa@Us@s@W_@g@_A[g@m@k@sAs@cA[m@GaA?_BBcBE_AMcBc@}@_@w@o@c@o@[m@[eAKwAK{@e@y@u@q@WS}AcAcAg@wBw@uAi@aAg@g@]EIe@We@hAa@~@QXWN{@Lo@Ms@Co@?iESWAe@B{@A{@Ai@Go@Oc@Ma@CaABa@BaBTu@Ay@Hc@Gk@_@s@IcAPe@R_@JsAHaBNoAVsAFmAb@_Al@S\\MNgBd@{@FsBTsCH[Du@cCMo@MaA?eAr@aH~@uJnAmJJi@JYZ_@dBuAdA^z@Lt@J^HNJLNFPD`@@PJ?NChB}@NAP@lAXRKHSFQHIz@Oh@KbCSlAOr@?N@DK'),
(76, 'Malabe Junction', '6.9039354', '79.9550129', 'Orugodawatta', '6.945244499999998', '79.881483', 'One Time Trip', '1:42 PM', '13.6 km', 'true', '2018-04-19', 1, 'slci@ye_gNGBEh@Q|A}@|Ce@|BIh@Aj@@\\BZ^fCJdA@f@EZ]~@iAzBiAnBuAhCARQd@_@rAkBdHu@xB]`@{@^iAl@qAz@a@^a@d@[v@g@~BMlA?b@F`@b@zARv@Jv@LpBHvB@lBAfEEzAE`B?RATAj@Al@?tABj@Lp@^hAbBrEd@|ARtAJjA@vBEbFIvBK`Ac@zBg@rC?TETEXAX@RNZ\\XTR^Jz@FhADf@Fj@Nh@RrA~@TVJ^BZE`@yArGGt@@\\H^T^TPz@j@x@r@fAtAnBfCl@`APf@~@tGTdBNf@\\x@`CrDTNv@vAHb@Ft@@lAA`F@xFGz@E`@IdAKbAe@dEuAfLGTWn@e@`AIl@CtBOxCAdAHp@~@|CHh@@`AOh@q@dA]f@{@x@_CtBcCvBY\\_AbAg@r@_@n@i@dBWdAWrBUfB_@tBo@tCoCpHe@`Ai@~@_CbE_@f@{AfDMb@Ix@@j@Jj@Tl@r@lAn@fATj@\\dATzADbAD`@Gn@q@vE_ApI]zCa@fCGT[x@g@n@oDlCy@x@IJMj@GZGlAGpCCfMIxJAZStAG`@Ah@Q`U{A?gHAiGGk@Ge@Im@C{HC{C?aNIwPUyFGsGCeAAkGGaQQ}GIoOOsROoGGsEDoB?wDGqECoA?{DHu@BUBGUi@{@K[EOCi@FQEw@KaA[qBc@{DKiAKQaAFq@D'),
(77, 'Malabe Junction', '6.9039354', '79.9550129', 'Pearson Lanka', '6.941507399999999', '79.8808706', 'One Time Trip', '1:48 PM', '13.9 km', 'true', '2018-04-19', 1, 'slci@ye_gNGBEh@Q|A}@|Ce@|BIh@Aj@@\\BZ^fCJdA@f@EZ]~@iAzBiAnBuAhCARQd@_@rAkBdHu@xB]`@{@^iAl@qAz@a@^a@d@[v@g@~BMlA?b@F`@b@zARv@Jv@LpBHvB@lBAfEEzAE`B?RATAj@Al@?tABj@Lp@^hAbBrEd@|ARtAJjA@vBEbFIvBK`Ac@zBg@rC?TETEXAX@RNZ\\XTR^Jz@FhADf@Fj@Nh@RrA~@TVJ^BZE`@yArGGt@@\\H^T^TPz@j@x@r@fAtAnBfCl@`APf@~@tGTdBNf@\\x@`CrDTNv@vAHb@Ft@@lAA`F@xFGz@E`@IdAKbAe@dEuAfLGTWn@e@`AIl@CtBOxCAdAHp@~@|CHh@@`AOh@q@dA]f@{@x@_CtBcCvBY\\_AbAg@r@_@n@i@dBWdAWrBUfB_@tBo@tCoCpHe@`Ai@~@_CbE_@f@{AfDMb@Ix@@j@Jj@Tl@r@lAn@fATj@\\dATzADbAD`@Gn@q@vE_ApI]zCa@fCGT[x@g@n@oDlCy@x@IJMj@GZGlAGpCCfMIxJAZStAG`@Ah@Q`U{A?gHAiGGk@Ge@Im@C{HC{C?aNIwPUyFGsGCeAAkGGaQQ}GIoOOsROoGGsEDoB?wDGqECoA?{DHu@BUBGUnAE~AIxAAdHBA_@OqAp@SRM^[NS@GQDKAGCmAcBoAyAlB{Ao@{@'),
(78, 'Sri Lanka Institute of Information Technology', '6.914834600000001', '79.973141', 'Pearson Lanka', '6.941507399999999', '79.8808706', 'One Time Trip', '11:18 PM', '16.5 km', 'true', '2018-04-19', 1, 'kqei@sqbgNt@DhCFrA?xCGx@@t@D~@JfBd@lEdAn@Tx@b@^XZ^Xb@Pb@jA`Dh@zAHb@Fr@IhAIf@Sp@Yv@K`@Gh@?f@LnA^vAJXXVh@\\p@ZvBbAnBdA~@p@z@x@h@x@\\|@jA`D|@zBbAdBj@z@d@~@fAtCv@zBh@nBx@rDRhAFn@?b@Gl@a@nBm@|BUdBSvCGnAQ|A}@|Ce@|BIh@Aj@@\\BZ^fCJdA@f@EZ]~@iAzBiAnBuAhCARQd@_@rAkBdHu@xB]`@{@^iAl@qAz@a@^a@d@[v@g@~BMlA?b@F`@b@zARv@Jv@LpBHvB@lBAfEEzAE`B?HAT?TAh@Al@@tA@T?JLp@^hAbBrEd@|ARtAJjA@vBEbFIvBK`Ac@zBg@rC?TETEXAX@RNZ\\XTR^Jz@FhADf@Fj@Nh@RrA~@TVJ^BZE`@yArGGt@@\\H^T^TPz@j@x@r@fAtAnBfCl@`APf@~@tGTdBNf@\\x@`CrDTNv@vAHb@Ft@@lAA`F@xFEr@CNC`@KdAKbAANa@lDuAfLGTWn@e@`AIl@CtBOxCAdAHp@~@|CHh@@`AOh@q@dA]f@{@x@_CtBcCvBY\\_AbAg@r@_@n@Sl@Uv@WdAm@zE_@tBo@tCoCpHe@`Ai@~@_CbE_@f@{AfDMb@Ix@@j@Jj@Tl@r@lAn@fATj@\\dATzADbAD`@Gn@q@vE_ApI]zCa@fCGT[x@g@n@oDlCy@x@IJMj@GZGlAGpC?lACxJIxJAZStAG`@Ah@Q`U{A?gHAiGGk@Ge@Im@C{HC{C?aNIwPUyFGsGCeAAkGGaQQ}GIoOOwOKkJKsEDoB?wDGqECoA?{DHu@BUBGUnAE~AIxAAdHBA_@OqAp@SRM^[NS@GQDKAGCmAcBoAyAlB{Ao@{@'),
(79, 'Sri Lanka Institute of Information Technology', '6.914834600000001', '79.973141', 'Pearson Lanka', '6.941507399999999', '79.8808706', 'One Time Trip', '11:00 PM', '16.5 km', 'true', '2018-04-22', 1, 'kqei@sqbgNt@DhCFrA?xCGx@@t@D~@JfBd@lEdAn@Tx@b@^XZ^Xb@Pb@jA`Dh@zAHb@Fr@IhAIf@Sp@Yv@K`@Gh@?f@LnA^vAJXXVh@\\p@ZvBbAnBdA~@p@z@x@h@x@\\|@jA`D|@zBbAdBj@z@d@~@fAtCv@zBh@nBx@rDRhAFn@?b@Gl@a@nBm@|BUdBSvCGnAQ|A}@|Ce@|BIh@Aj@@\\BZ^fCJdA@f@EZ]~@iAzBiAnBuAhCARQd@_@rAkBdHu@xB]`@{@^iAl@qAz@a@^a@d@[v@g@~BMlA?b@F`@b@zARv@Jv@LpBHvB@lBAfEEzAE`B?HAT?TAh@Al@@tA@T?JLp@^hAbBrEd@|ARtAJjA@vBEbFIvBK`Ac@zBg@rC?TETEXAX@RNZ\\XTR^Jz@FhADf@Fj@Nh@RrA~@TVJ^BZE`@yArGGt@@\\H^T^TPz@j@x@r@fAtAnBfCl@`APf@~@tGTdBNf@\\x@`CrDTNv@vAHb@Ft@@lAA`F@xFEr@CNC`@KdAKbAANa@lDuAfLGTWn@e@`AIl@CtBOxCAdAHp@~@|CHh@@`AOh@q@dA]f@{@x@_CtBcCvBY\\_AbAg@r@_@n@Sl@Uv@WdAm@zE_@tBo@tCoCpHe@`Ai@~@_CbE_@f@{AfDMb@Ix@@j@Jj@Tl@r@lAn@fATj@\\dATzADbAD`@Gn@q@vE_ApI]zCa@fCGT[x@g@n@oDlCy@x@IJMj@GZGlAGpC?lACxJIxJAZStAG`@Ah@Q`U{A?gHAiGGk@Ge@Im@C{HC{C?aNIwPUyFGsGCeAAkGGaQQ}GIoOOwOKkJKsEDoB?wDGqECoA?{DHu@BUBGUnAE~AIxAAdHBA_@OqAp@SRM^[NS@GQDKAGCmAcBoAyAlB{Ao@{@'),
(80, 'Sri Lanka Institute of Information Technology', '6.914834600000001', '79.973141', 'Orugodawatta', '6.945244499999998', '79.881483', 'One Time Trip', '11:22 PM', '16.2 km', 'true', '2018-04-19', 1, 'kqei@sqbgNt@DhCFrA?xCGx@@t@D~@JfBd@lEdAn@Tx@b@^XZ^Xb@Pb@jA`Dh@zAHb@Fr@IhAIf@Sp@Yv@K`@Gh@?f@LnA^vAJXXVh@\\p@ZvBbAnBdA~@p@z@x@h@x@\\|@jA`D|@zBbAdBj@z@d@~@fAtCv@zBh@nBx@rDRhAFn@?b@Gl@a@nBm@|BUdBSvCGnAQ|A}@|Ce@|BIh@Aj@@\\BZ^fCJdA@f@EZ]~@iAzBiAnBuAhCARQd@_@rAkBdHu@xB]`@{@^iAl@qAz@a@^a@d@[v@g@~BMlA?b@F`@b@zARv@Jv@LpBHvB@lBAfEEzAE`B?HAT?TAh@Al@@tA@T?JLp@^hAbBrEd@|ARtAJjA@vBEbFIvBK`Ac@zBg@rC?TETEXAX@RNZ\\XTR^Jz@FhADf@Fj@Nh@RrA~@TVJ^BZE`@yArGGt@@\\H^T^TPz@j@x@r@fAtAnBfCl@`APf@~@tGTdBNf@\\x@`CrDTNv@vAHb@Ft@@lAA`F@xFEr@CNC`@KdAKbAANa@lDuAfLGTWn@e@`AIl@CtBOxCAdAHp@~@|CHh@@`AOh@q@dA]f@{@x@_CtBcCvBY\\_AbAg@r@_@n@Sl@Uv@WdAm@zE_@tBo@tCoCpHe@`Ai@~@_CbE_@f@{AfDMb@Ix@@j@Jj@Tl@r@lAn@fATj@\\dATzADbAD`@Gn@q@vE_ApI]zCa@fCGT[x@g@n@oDlCy@x@IJMj@GZGlAGpC?lACxJIxJAZStAG`@Ah@Q`U{A?gHAiGGk@Ge@Im@C{HC{C?aNIwPUyFGsGCeAAkGGaQQ}GIoOOwOKkJKsEDoB?wDGqECoA?{DHu@BUBGUi@{@K[EOCi@FQEw@KaA[qBc@{DKiAKQaAFq@D'),
(81, 'Sri Lanka Institute of Information Technology', '6.914834600000001', '79.973141', 'Borella Junction', '6.9117165', '79.87744889999999', 'Daily', '11:25 PM', '12.2 km', 'true', '', 1, 'kqei@sqbgNt@DhCFrA?xCGx@@t@D~@JfBd@lEdAn@Tx@b@^XZ^Xb@Pb@jA`Dh@zAHb@Fr@IhAIf@Sp@Yv@K`@Gh@?f@LnA^vAJXXVh@\\p@ZvBbAnBdA~@p@z@x@h@x@\\|@jA`D|@zBbAdBj@z@d@~@fAtCv@zBh@nBx@rDRhAFn@?b@Gl@a@nBm@|BUdBSvCGnAQ|A}@|Ce@|BIh@Aj@@\\BZ^fCJdA@f@EZ]~@iAzBiAnBuAhCARQd@_@rAkBdHu@xB]`@{@^iAl@qAz@a@^a@d@[v@g@~BMlA?b@F`@b@zARv@Jv@LpBHvB@lBAfEEzAE`B?HAT?TAh@Al@@tA@T?JLp@^hAbBrEd@|ARtAJjA@vBEbFIvBK`Ac@zBg@rC?TETEXAX@RNZ\\XTR^Jz@FhADf@Fj@Nh@RrA~@TVJ^BZE`@yArGGt@@\\H^T^TPz@j@x@r@fAtAnBfCl@`APf@~@tGTdBNf@\\x@`CrDTNv@vAHb@Ft@@lAA`F@xFEr@CNC`@KdAKbAANa@lDuAfLGTWn@e@`AIl@CtBOxCAdAHp@~@|CHh@@`AOh@q@dA]f@{@x@_CtBcCvBY\\_AbAg@r@_@n@Sl@Uv@WdAm@zE_@tBo@tCoCpHe@`Ai@~@_CbE_@f@{AfDMb@Ix@@j@Jj@Tl@r@lAn@fATj@\\dATzADbAD`@Gn@q@vE_ApI]zCa@fCGT[x@g@n@oDlCy@x@IJMj@GZGlAGpC?lACxJIxJAZStAG`@Ah@Q`U{A?G?'),
(82, 'Sri Lanka Institute of Information Technology', '6.914834600000001', '79.973141', 'Orugodawatta', '6.945244499999998', '79.881483', 'One Time Trip', '11:32 PM', '16.2 km', 'true', '2018-04-19', 1, 'kqei@sqbgNt@DhCFrA?xCGx@@t@D~@JfBd@lEdAn@Tx@b@^XZ^Xb@Pb@jA`Dh@zAHb@Fr@IhAIf@Sp@Yv@K`@Gh@?f@LnA^vAJXXVh@\\p@ZvBbAnBdA~@p@z@x@h@x@\\|@jA`D|@zBbAdBj@z@d@~@fAtCv@zBh@nBx@rDRhAFn@?b@Gl@a@nBm@|BUdBSvCGnAQ|A}@|Ce@|BIh@Aj@@\\BZ^fCJdA@f@EZ]~@iAzBiAnBuAhCARQd@_@rAkBdHu@xB]`@{@^iAl@qAz@a@^a@d@[v@g@~BMlA?b@F`@b@zARv@Jv@LpBHvB@lBAfEEzAE`B?HAT?TAh@Al@@tA@T?JLp@^hAbBrEd@|ARtAJjA@vBEbFIvBK`Ac@zBg@rC?TETEXAX@RNZ\\XTR^Jz@FhADf@Fj@Nh@RrA~@TVJ^BZE`@yArGGt@@\\H^T^TPz@j@x@r@fAtAnBfCl@`APf@~@tGTdBNf@\\x@`CrDTNv@vAHb@Ft@@lAA`F@xFEr@CNC`@KdAKbAANa@lDuAfLGTWn@e@`AIl@CtBOxCAdAHp@~@|CHh@@`AOh@q@dA]f@{@x@_CtBcCvBY\\_AbAg@r@_@n@Sl@Uv@WdAm@zE_@tBo@tCoCpHe@`Ai@~@_CbE_@f@{AfDMb@Ix@@j@Jj@Tl@r@lAn@fATj@\\dATzADbAD`@Gn@q@vE_ApI]zCa@fCGT[x@g@n@oDlCy@x@IJMj@GZGlAGpC?lACxJIxJAZStAG`@Ah@Q`U{A?gHAiGGk@Ge@Im@C{HC{C?aNIwPUyFGsGCeAAkGGaQQ}GIoOOwOKkJKsEDoB?wDGqECoA?{DHu@BUBGUi@{@K[EOCi@FQEw@KaA[qBc@{DKiAKQaAFq@D'),
(83, 'Sri Lanka Institute of Information Technology', '6.914834600000001', '79.973141', 'Orugodawatta', '6.945244499999998', '79.881483', 'Daily', '11:30 AM', '16.2 km', 'true', '', 1, 'kqei@sqbgNt@DhCFrA?xCGx@@t@D~@JfBd@lEdAn@Tx@b@^XZ^Xb@Pb@jA`Dh@zAHb@Fr@IhAIf@Sp@Yv@K`@Gh@?f@LnA^vAJXXVh@\\p@ZvBbAnBdA~@p@z@x@h@x@\\|@jA`D|@zBbAdBj@z@d@~@fAtCv@zBh@nBx@rDRhAFn@?b@Gl@a@nBm@|BUdBSvCGnAQ|A}@|Ce@|BIh@Aj@@\\BZ^fCJdA@f@EZ]~@iAzBiAnBuAhCARQd@_@rAkBdHu@xB]`@{@^iAl@qAz@a@^a@d@[v@g@~BMlA?b@F`@b@zARv@Jv@LpBHvB@lBAfEEzAE`B?HAT?TAh@Al@@tA@T?JLp@^hAbBrEd@|ARtAJjA@vBEbFIvBK`Ac@zBg@rC?TETEXAX@RNZ\\XTR^Jz@FhADf@Fj@Nh@RrA~@TVJ^BZE`@yArGGt@@\\H^T^TPz@j@x@r@fAtAnBfCl@`APf@~@tGTdBNf@\\x@`CrDTNv@vAHb@Ft@@lAA`F@xFEr@CNC`@KdAKbAANa@lDuAfLGTWn@e@`AIl@CtBOxCAdAHp@~@|CHh@@`AOh@q@dA]f@{@x@_CtBcCvBY\\_AbAg@r@_@n@Sl@Uv@WdAm@zE_@tBo@tCoCpHe@`Ai@~@_CbE_@f@{AfDMb@Ix@@j@Jj@Tl@r@lAn@fATj@\\dATzADbAD`@Gn@q@vE_ApI]zCa@fCGT[x@g@n@oDlCy@x@IJMj@GZGlAGpC?lACxJIxJAZStAG`@Ah@Q`U{A?gHAiGGk@Ge@Im@C{HC{C?aNIwPUyFGsGCeAAkGGaQQ}GIoOOwOKkJKsEDoB?wDGqECoA?{DHu@BUBGUi@{@K[EOCi@FQEw@KaA[qBc@{DKiAKQaAFq@D'),
(84, 'Sri Lanka Institute of Information Technology', '6.914834600000001', '79.973141', 'Kandy', '7.290571500000001', '80.6337262', 'One Time Trip', '11:38 PM', '109.8 km', 'true', '2018-04-19', 1, 'arei@uqbgN}HcBsEqF{ScDyHyNaLiFeC`CwDJoQ_A}NDaQhE_FdAiCRaArB|@nG|@vNZnCqHi@cF_@iHeA{_@{Myj@oJoZB}i@jHyy@xWo~Anf@wr@tNqTbEy@dDoD\\iF@QmDgCwFeUwQqM}EaRqBkImCeDsGyNsHyNuGeIwKsDsIaM_Mke@ai@sPiVgIeNeQqGmNgAmGaG_EcGoCeFyFwAiIgJiDwGeYqUiQwS}D_H_H@wKwBmNmG_`A{YqVuXwHuNuMg_@_IaKc^y_@yHyGwX_KqOoMeTgMqXo@}Hc@uEwEqBaFuHqD_IsBsDuJVeJcBiFsQsKmJ_M}KmIyM_Wsc@_w@ml@sq@mJ_TkAmWuHa\\_LwVy_@mMqPaFoWiUmP}VmG{OmG_PgAoHuNgo@gS_Bcc@mUsC_HsNuB}^mHwEcGaNwRyQyAiJwHcO}CqDyMOiTtQiMbLqBzA}GkE}\\eDiCeRkAoE{IqJkAuLgDsEVeKyByZcSuDiT}A{YaBuJh@gWaEyLka@_]iT{SgT{Lgg@ye@wWkm@kI}SuQ}LgNeQcK}F_JmJeTyEyFAiGyHj@cM|TqNfU{IpB_DxLqB~A}BaGeFeSoZkF_CcBuGeEgLsDmOaAsPr]gUhCiGe@cMbMcSnAgEqAkFuDwNqGsx@cIqj@nByOoMqI_BeIn@iJuA}]lAcXi@}S|CqQtAmYgE{b@d@iWxJqs@hCyMuAgG@eDyFkBcGwFxDeChBuJoG_S_Y@oI`@cAsCoQ{DyKrAoNc@qBuJnE_k@{Jav@eEot@uDaRkAwD~B}KzJuVvFkJdC_Iw@c^{A}\\iT{uA{Cs\\wGkZTkQYaUrB{c@pFg^lCkMhDmB`OuD~LgD|FaD{NaIbBwA}@aBmEF_BqEkC{Y?aOE_e@sFw_@{Ey[fCsR`NoMrK{c@CcV{CuLtImMnFkDoAoHSkAW{HwAwOlAoCuBoKsDuP_CoQsGiZuEsAs@yDaCiBj@sCyD{Aq@_CvAi@kA_@}BbAmDyCsAkKvBcJvK{RjCsKg@}GuBiMoAsD`EkN_@gWJeC`CR|Dh@kIwEwKc_@uD_E{DgOcJae@kCkM}BkCcCoDeAoBmBcEiAmCW}FfCgNaBqTpDmJi@wG}BiJmI{IXcSeNiYXsLzBwMlCiJM_Q}GiQeDqOmEyNpAaIaAc]uCuHy@yOvJmFnHyR|DyJy@eAgEc[CaFlHs@K}GcFgGqMcEgCkEtCcM^aHoHiO_O_HUQPWz@kFyRgd@c]{g@m`@wn@qReY_EcFqKwCsEwCzGyA`@pA'),
(85, 'Sri Lanka Institute of Information Technology', '6.914834600000001', '79.973141', 'Orugodawatta', '6.945244499999998', '79.881483', 'One Time Trip', '10:10 AM', '16.2 km', 'true', '2018-04-30', 1, 'kqei@sqbgNt@DhCFrA?xCGx@@t@D~@JfBd@lEdAn@Tx@b@^XZ^Xb@Pb@jA`Dh@zAHb@Fr@IhAIf@Sp@Yv@K`@Gh@?f@LnA^vAJXXVh@\\p@ZvBbAnBdA~@p@z@x@h@x@\\|@jA`D|@zBbAdBj@z@d@~@fAtCv@zBh@nBx@rDRhAFn@?b@Gl@a@nBm@|BUdBSvCGnAQ|A}@|Ce@|BIh@Aj@@\\BZ^fCJdA@f@EZ]~@iAzBiAnBuAhCARQd@_@rAkBdHu@xB]`@{@^iAl@qAz@a@^a@d@[v@g@~BQlA?b@Bb@Hb@Xv@Pl@N~@RrDFbCB`ECtAIhC?JAT?RAj@Al@@tAFbAZbAvAvDt@zBRbANrADrB?rAI|FIlA]rB_@hBOv@EZ?TIb@Cn@BPFLZZNJLJNFZDb@DbBFp@Ld@Nj@Xd@Zd@`@PZF\\A\\oAtFSrAA`@BZN^VXx@j@v@l@b@h@nClDl@t@LRRh@H`@p@zEb@xC^bAR^dBlCPXPF|@`BJ~@@h@A`EBfGA~ACXAF?FCPANE`@Eb@IbACNUxBeBhNUl@c@|@Ol@IvDOtCF|@~@zCLl@@`AIh@Ub@s@bAo@r@wApAcEpD_A`Ay@fAa@n@Sl@Ur@YdAm@vE[pBq@xCOn@yAzD_A|Bc@v@oC~Ea@h@]r@{@pBO\\Kl@Cr@Dh@Nj@b@v@|@zAXj@Xt@\\|ARjCC`@G`@m@dEWpCeAzIe@rCYx@g@p@uCzBqAfAQVSx@Gn@CjCEvAA~IGzJEpAGn@Q|@Cb@OfUARO?sA?sOGkAK[Gm@CeJCmJEyMMq@AyIMaKEqHEmII{EImECuDG{MMmPO{GGkGAeDBmEGcGCqB@oBFcAFGUi@{@K_@Iu@FQA]K_AQwAWaBSgBQqBKQ_@BcAFO@');

-- --------------------------------------------------------

--
-- Table structure for table `driver_waypoints`
--

CREATE TABLE `driver_waypoints` (
  `id` int(11) UNSIGNED NOT NULL,
  `routeId` int(11) DEFAULT NULL,
  `location` varchar(300) DEFAULT NULL,
  `latitude` varchar(300) DEFAULT NULL,
  `longitude` varchar(300) DEFAULT NULL,
  `waypointPosition` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `route_requests`
--

CREATE TABLE `route_requests` (
  `requestId` int(11) UNSIGNED NOT NULL,
  `routeId` int(11) DEFAULT NULL,
  `passengerId` int(11) DEFAULT NULL,
  `driverId` int(11) DEFAULT NULL,
  `isAccepted` int(11) NOT NULL DEFAULT '0' COMMENT '0 - Rejected / 1 - Accpeted / 3- Expired'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `route_requests`
--

INSERT INTO `route_requests` (`requestId`, `routeId`, `passengerId`, `driverId`, `isAccepted`) VALUES
(1, 85, 27, 1, 0),
(2, 85, 27, 1, 0),
(3, 85, 27, 1, 0),
(4, 85, 27, 1, 0),
(5, 85, 27, 1, 0),
(6, 85, 27, 1, 0),
(7, 85, 27, 1, 0),
(8, 85, 27, 1, 0),
(9, 85, 27, 1, 0),
(10, 85, 27, 1, 0),
(11, 85, 27, 1, 0),
(12, 85, 27, 1, 0),
(13, 85, 27, 1, 0),
(14, 85, 27, 1, 0),
(15, 85, 27, 1, 0),
(16, 85, 27, 1, 0),
(17, 85, 27, 1, 0),
(18, 85, 27, 1, 0),
(19, 85, 27, 1, 0),
(20, 85, 27, 1, 0),
(21, 85, 27, 1, 0),
(22, 85, 27, 1, 0),
(23, 85, 27, 1, 0),
(24, 85, 27, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `u_userId` int(20) UNSIGNED NOT NULL,
  `u_firstName` varchar(200) DEFAULT NULL,
  `u_lastName` varchar(200) DEFAULT NULL,
  `u_contactNumber` varchar(200) DEFAULT NULL,
  `u_email` varchar(200) DEFAULT NULL,
  `u_password` varchar(200) DEFAULT NULL,
  `u_nicNumber` varchar(200) DEFAULT NULL,
  `u_gender` varchar(200) DEFAULT NULL,
  `u_firebaseToken` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`u_userId`, `u_firstName`, `u_lastName`, `u_contactNumber`, `u_email`, `u_password`, `u_nicNumber`, `u_gender`, `u_firebaseToken`) VALUES
(1, 'Sameera', 'Driver', '0170589523', 'sam@gmail.com', '123123123', '12312', 'Male', 'fZwuJ6ZW26c:APA91bGuSpNYAeqd2OzuzWLGkk9r9dA2rxBGpsSVHmj9HoCqWUKRM_auNiG-8c_Z9awKr6-7U9BR38VufaDZRZJ9hn5vBAfHm9TEugo_TOdm6Ka7kSgrt8766z0tveizbsb_5JzI32Yn'),
(27, 'Aseni', 'Passenger', '0170589523', 'aseni@gmail.com', 'sasasasa', '12312', 'Female', 'fUfoPgfZVeY:APA91bFHYlPFByxwrzF9DvNRis7EYxxdkpPVP-VGLZ_HqPi01g8onDPei2o592PFEpAUJDAqQCzIG3BSnHffUuzxDhqlB-eH98svyUn3gPo-AQm2-YP2FVJH9Qbq0IMD8Keo-GPn3G3_');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `driver_routes`
--
ALTER TABLE `driver_routes`
  ADD PRIMARY KEY (`routeId`);

--
-- Indexes for table `driver_waypoints`
--
ALTER TABLE `driver_waypoints`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `route_requests`
--
ALTER TABLE `route_requests`
  ADD PRIMARY KEY (`requestId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`u_userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `driver_routes`
--
ALTER TABLE `driver_routes`
  MODIFY `routeId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `driver_waypoints`
--
ALTER TABLE `driver_waypoints`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `route_requests`
--
ALTER TABLE `route_requests`
  MODIFY `requestId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `u_userId` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
